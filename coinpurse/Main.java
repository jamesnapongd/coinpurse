package coinpurse;

import coinpurse.strategy.GreedyWithdraw;


/**
 * A main class to create objects and connect objects together.
 * The user interface needs a reference to coin purse.
 * @author Napong Dungduangsasitorn
 */
public class Main {

	private static int CAPACITY = 10;
	/**
	 * @param args not used
	 */
	public static void main( String[] args ) {

		// 1. create a Purse
		Purse purse = new Purse(CAPACITY);
		// 2. create a ConsoleDialog with a reference to the Purse object
		ConsoleDialog coinConsole = new ConsoleDialog(purse);
		// 3. run() the ConsoleDialog
		coinConsole.run();
		
		GreedyWithdraw greedy = new GreedyWithdraw();
	}
}
