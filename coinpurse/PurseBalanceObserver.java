package coinpurse;

import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class PurseBalanceObserver implements Observer {
	
	
	@Override
	public void update(Observable subject, Object info) {
		// TODO Auto-generated method stub
		if(subject instanceof Purse){
			Purse purse = (Purse)subject;
			double balance = purse.getBalance();
			System.out.println("Balance is: " + balance);
		}
		if(info != null )System.out.println( info );
	}
	

	

}
