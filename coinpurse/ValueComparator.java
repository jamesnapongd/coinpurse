package coinpurse;

import java.util.Comparator;

/**
 * ValueComparater compare valuable object.
 * @author Napong Dungduangsasitorn
 *
 */
public class ValueComparator implements Comparator<Valuable> {
	/**
	 * compare valuable object.
	 * @param a first valuable.
	 * @param b second valuable.
	 * @return return < 0 if first valuable is less than second one, 
	 * > 0 if first valuable is greater than second one,
	 * or 0 if two valuable is equal
	 */
	public int compare(Valuable a, Valuable b) {
		if (a.getValue() < b.getValue()){
			return -1;
		}
		
		else if(a.getValue() > b.getValue()){
			return 1;
		}
		return 0;
	}
}
