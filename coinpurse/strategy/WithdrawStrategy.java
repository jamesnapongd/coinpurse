package coinpurse.strategy;

import java.util.List;
import coinpurse.Valuable;

/**
 * interface withdraw strategy.
 * @author Napong Dungduangsasitorn
 *
 *
 */

public interface WithdrawStrategy {

	/**
	 * withdraw money.
	 * @param amount of value. 
	 * @param valuables of money.
	 * @return list of coin banknote coupon.
	 */
	public Valuable[] withdraw(double amount, List<Valuable> valuables);

}
