package coinpurse;

/**
 * A coin with a monetary value.
 * You can't change the value of a coin.
 * @author Napong Dungduangsasitorn
 */
public class Coin extends AbstractValuable {


	/** 
	 * Constructor for a new coin. 
	 * @param value is the value for the coin
	 */
	public Coin( double value ) {
		super(value);
	}
	
	
	
	/**
	 * print value of coin.
	 * @return String description of Coin, such as "5-Baht coin".
	 */
	public String toString(){
		return String.format("%.2f-Baht coin", this.getValue());
	}


}
