package coinpurse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;

import coinpurse.strategy.GreedyWithdraw;
import coinpurse.strategy.WithdrawStrategy;
/**
 *  A coin purse contains coins.
 *  You can insert coins, withdraw money, check the balance,
 *  and check if the purse is full.
 *  When you withdraw money, the coin purse decides which
 *  coins to remove.
 *  
 * @author Napong Dungduangsasitorn
 */
public class Purse extends Observable{
	/** Collection of coins in the purse. */
	private ArrayList<Valuable> money;
	/** Capacity is maximum NUMBER of coins the purse can hold.
	 *  Capacity is set when the purse is created.
	 */
	private int capacity;
	
	
	/**
	 * withdraw with strategy.
	 */
	private WithdrawStrategy strategy;
	/** 
	 *  Create a purse with a specified capacity.
	 *  @param capacity is maximum number of coins you can put in purse.
	 */
	public Purse( int capacity ) {
		money = new ArrayList<Valuable>();
		this.capacity = capacity;
	}

	/**
	 * Count and return the number of coins in the purse.
	 * This is the number of coins, not their value.
	 * @return coins size the number of coins in the purse
	 */
	public int count() { 
		return this.money.size(); 
	}

	/** 
	 *  Get the total value of all items in the purse.
	 *  @return balance the total value of items in the purse.
	 */
	public double getBalance() {
		double balance = 0;
		for(int i = 0 ; i < money.size() ; i++){
			balance +=money.get(i).getValue();
			System.out.println(money.get(i).getValue());
		}
		return balance; 
	}

	/**
	 * Return the capacity of the coin purse.
	 * @return the capacity
	 */

	public int getCapacity() { 
		return this.capacity; 
	}

	/** 
	 *  Test whether the purse is full.
	 *  The purse is full if number of items in purse equals
	 *  or greater than the purse capacity.
	 *  @return true if purse is full.
	 */
	public boolean isFull() {
		if(this.capacity == money.size()){
			return true;
		}
		return false;
	}

	/** 
	 * Insert a money into the purse.
	 * The money is only inserted if the purse has space for it
	 * and the money has positive value.  No worthless money!
	 * @param valueable is a money object to insert into purse
	 * @return true if money inserted, false if can't insert
	 */
	public boolean insert(Valuable valueable){
		// if the purse is already full then can't insert anything.
		if(!isFull()&&valueable.getValue() >= 0){
			return true;
		}
		money.add(valueable);
		super.setChanged();
		super.notifyObservers(this);
		return false;
	}


	/**  
	 *  Withdraw the requested amount of money.
	 *  @param amount is the amount to withdraw
	 *  @return array of Coin objects for money withdrawn, 
	 *    or null if cannot withdraw requested amount.
	 */
	public Valuable[] withdraw( double amount ) {
		
		Valuable[] withdrawMoney = strategy.withdraw(amount, money);
		if(amount < 0){
			return null;
		}
		if(withdrawMoney != null){
			int index = 0;
			for(index = 0 ;index < withdrawMoney.length ; index++);{
				money.remove(withdrawMoney[index]);
			}
		}
		return null;
	
	}

	/** 
	 * toString returns a string.
	 * @return string.
	 */
	public String toString() {
		return String.format("%d coins with value %.2f", money.size(), this.getBalance());
	}

	/**
	 * set withdraw strategy.
	 * @param withdrawStrategy how to withdraw.
	 */
	public void setWithdrawStategy(WithdrawStrategy withdrawStrategy ){
		this.strategy = withdrawStrategy;
	}

	
	

}
