
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Practice recursion.
 * @author Napong Dungduangsasitorn
 *
 */
public class ListUtil {

	static String temp = "";
	private static void printList(List<?> list) {
		if(list.size() != 0 ){
			System.out.print(list.get(0));
			list = list.subList(1, list.size());
			if(list.size() >= 1)
				System.out.print(", ");
			printList(list);

		}
	}

	/**
	 * Find the largest element in a List of Strings,
	 * using the String compareTo method.
	 * @return the lexically largest element in the List
	 */

	private static String max( List<String> list) {

		if( list.size() != 0){
			if( list.get(0).compareTo(temp) > 0){
				temp = list.get(0);
			}

			list = list.subList(1, list.size());
			max(list);
		}
		return temp;
	}
	/** Test the max method. */

	/**
	 * @param args not use.
	 * 
	 */
	public static void main(String [] args) {
		List<String> list;
		// if any command line args, then use them as the list!
		if (args.length > 0) list = Arrays.asList( args );
		else list = Arrays.asList("bird", "cat", "zebra", "pig");
		System.out.print("List contains: ");
		printList( list );
		String max = max(list);
		System.out.println();
		System.out.println("Lexically greatest element is "+max);
	}


}